

def tensoriel2x2(m1, w1, m2, w2):
  m3 = []
  m4 = []
  for i in range(len(m1)//w1):
    m3.append([])
    for j in range(w1):
      m3[i].append([])
      for k in range(len(m2)//w2):
        m3[i][j].append([])
        for l in range(w2):
          r = (m1[i * w1 + j][0] * m2[k * w2 + l][0]) // 255
          g = (m1[i * w1 + j][1] * m2[k * w2 + l][1]) // 255
          b = (m1[i * w1 + j][2] * m2[k * w2 + l][2]) // 255
          m3[i][j][k].append((r, g, b))

  return m3
