#!/bin/bash

cd output
ffmpeg -f image2 -r 5 -i imgA*.png -vcodec mpeg4 -y faceA.mp4
ffmpeg -f image2 -r 5 -i imgB*.png -vcodec mpeg4 -y faceB.mp4
ffmpeg -f image2 -r 5 -i imgC*.png -vcodec mpeg4 -y faceC.mp4
ffmpeg -f image2 -r 5 -i imgD*.png -vcodec mpeg4 -y faceD.mp4
ffmpeg -f image2 -r 5 -i imgE*.png -vcodec mpeg4 -y faceE.mp4
ffmpeg -f image2 -r 5 -i imgF*.png -vcodec mpeg4 -y faceF.mp4
cd ..
