#! /bin/python3
#import my_IO as io
import sys
import struct
from PIL.Image import *
import tensorial

def generate_faceA(dir_name, im):
  for t in range(len(im[0][0][0])):
# 4eme dim = time
    data1  = []
    data2  = []
    p = len(im[0][0]) -1
    for i in range(len(im)):
      for j in range(len(im[0])):
        data1.append(int(im[i][j][0][t][0]))
        data1.append(int(im[i][j][0][t][1]))
        data1.append(int(im[i][j][0][t][2]))
        data2.append(int(im[i][j][p][t][0]))
        data2.append(int(im[i][j][p][t][1]))
        data2.append(int(im[i][j][p][t][2]))
    data1 = bytes(data1)
    filename = dir_name + "/A_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im), len(im[0])), data1, 'raw', 'RGB', 0, 1)
    ima.save(filename)
    
    data2 = bytes(data2)
    filename = dir_name + "/F_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im), len(im[0])), data2, 'raw', 'RGB', 0, 1)
    ima.save(filename)

def generate_faceB(dir_name, im):
  for t in range(len(im[0][0][0])):
# 4eme dim = time
    data1  = []
    data2  = []
    p = len(im[0]) -1
    for i in range(len(im)):
      for j in range(len(im[0][0])):
        data1.append(int(im[i][0][j][t][0]))
        data1.append(int(im[i][0][j][t][1]))
        data1.append(int(im[i][0][j][t][2]))
        data2.append(int(im[i][p][j][t][0]))
        data2.append(int(im[i][p][j][t][1]))
        data2.append(int(im[i][p][j][t][2]))
    data1 = bytes(data1)
    filename = dir_name + "/B_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im), len(im[0][0])), data1, 'raw', 'RGB', 0, 1)
    ima.save(filename)
    
    data2 = bytes(data2)
    filename = dir_name + "/E_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im), len(im[0][0])), data2, 'raw', 'RGB', 0, 1)
    ima.save(filename)

def generate_faceC(dir_name, im):
  for t in range(len(im[0][0][0])):
# 4eme dim = time
    data1  = []
    data2  = []
    p = len(im) -1
    for i in range(len(im[0])):
      for j in range(len(im[0][0])):
        data1.append(int(im[0][i][j][t][0]))
        data1.append(int(im[0][i][j][t][1]))
        data1.append(int(im[0][i][j][t][2]))
        data2.append(int(im[p][i][j][t][0]))
        data2.append(int(im[p][i][j][t][1]))
        data2.append(int(im[p][i][j][t][2]))
    data1 = bytes(data1)
    filename = dir_name + "/C_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im[0]), len(im[0][0])), data1, 'raw', 'RGB', 0, 1)
    ima.save(filename)
    
    data2 = bytes(data2)
    filename = dir_name + "/D_" + str(10000+t) + ".tif"
    ima = frombuffer('RGB', (len(im[0]), len(im[0][0])), data2, 'raw', 'RGB', 0, 1)
    ima.save(filename)

def main(argv):
  if len(argv) < 4:
      print("usage : main.py input_file1 input_file2")
      exit(1)
  im1 = open(argv[1])
  im2 = open(argv[2])
  print("Produit tensoriel...")
  im3 = tensorial.tensoriel2x2(im1.getdata(), im1.size[0],
        im2.getdata(), im2.size[0])
  
  print("Générer les images...")
  generate_faceA("output", im3)
  generate_faceB("output", im3)
  generate_faceC("output", im3)

if __name__ == "__main__":
    main(sys.argv)
